package com.example.playtogether;

import android.app.Application;
import android.util.Log;

import com.huawei.agconnect.cloud.database.AGConnectCloudDB;

public class MainApplication extends Application {
    private static final String TAG = MainApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        Log.d(TAG, "before onCreate");
        super.onCreate();
        Log.d(TAG, "after onCreate");

        //初始化云数据库
//        AGConnectCloudDB.initialize(getApplicationContext());

    }


}
