package com.example.playtogether;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.huawei.agconnect.AGCRoutePolicy;
import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.AGConnectOptionsBuilder;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.cloud.database.AGConnectCloudDB;
import com.huawei.agconnect.cloud.database.CloudDBZone;
import com.huawei.agconnect.cloud.database.CloudDBZoneConfig;
import com.huawei.agconnect.cloud.database.CloudDBZoneObjectList;
import com.huawei.agconnect.cloud.database.CloudDBZoneQuery;
import com.huawei.agconnect.cloud.database.CloudDBZoneSnapshot;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = MainActivity.class.getSimpleName();
    //初始化数据库
    AGConnectCloudDB mCloudDB;
    //初始化数据库配置
    CloudDBZoneConfig mConfig;
    //初始化存储区
    CloudDBZone mCloudDBZone;
    //初始化表格

    //云端数据对象List
    private ArrayList<SportAdapter.SportItem> mSportList = new ArrayList<SportAdapter.SportItem>();

    private Button btnSignout;
    private RecyclerView rvSportList;
    private SportAdapter mSportAdapter;

    private ImageView imgBack;
    private ImageView imgAdd;


    public interface UiCallBack {
        void onDelete(SportInfo info);

        void onModify(SportInfo info);
    }

    private UiCallBack mUiCallBack = new UiCallBack() {
        @Override
        public void onDelete(SportInfo info) {

        }

        @Override
        public void onModify(SportInfo info) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.statuscolor));
        }

        rvSportList = findViewById(R.id.rv_list);



        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(this);
        imgAdd = findViewById(R.id.img_add);
        imgAdd.setOnClickListener(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                openAndQueryCloudDB();

                mSportAdapter = new SportAdapter(mSportList, mUiCallBack);
                rvSportList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                rvSportList.setAdapter(mSportAdapter);
            }
        }, 500);

    }

    private void openAndQueryCloudDB() {
        //初始化云数据库
        AGConnectCloudDB.initialize(getApplicationContext());
        AGConnectInstance instance = AGConnectInstance.buildInstance(new AGConnectOptionsBuilder().setRoutePolicy(AGCRoutePolicy.CHINA).build(getApplicationContext()));
//        mCloudDB = AGConnectCloudDB.getInstance(instance, AGConnectAuth.getInstance(instance));
        mCloudDB = AGConnectCloudDB.getInstance();
        Log.i(TAG,"The cloudDB is" + mCloudDB);
        try {
            mCloudDB.createObjectType(ObjectTypeInfoHelper.getObjectTypeInfo());
            mConfig = new CloudDBZoneConfig("SportDB",
                    CloudDBZoneConfig.CloudDBZoneSyncProperty.CLOUDDBZONE_CLOUD_CACHE,
                    CloudDBZoneConfig.CloudDBZoneAccessProperty.CLOUDDBZONE_PUBLIC);
            mConfig.setPersistenceEnabled(true);
            Task<CloudDBZone> openDBZoneTask = mCloudDB.openCloudDBZone2(mConfig, true);
            openDBZoneTask.addOnSuccessListener(new OnSuccessListener<CloudDBZone>() {
                @Override
                public void onSuccess(CloudDBZone cloudDBZone) {
                    Log.i("open clouddbzone", "open cloudDBZone success");
                    mCloudDBZone = cloudDBZone;
                    GlobalInfo.gCloudDBZone = cloudDBZone;
                    updateData();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Log.w("open clouddbzone", "open cloudDBZone failed for " + e.getMessage());
                }
            });
        } catch (AGConnectCloudDBException e) {
            Toast.makeText(MainActivity.this, "initialize CloudDB failed" + e, Toast.LENGTH_LONG).show();
        }
    }

    private void updateData() {
        Log.d(TAG, "zzz, bindData was called.");

        CloudDBZoneQuery<SportInfo> query = CloudDBZoneQuery.where(SportInfo.class);
        querySportInfo(query);
    }

    private void querySportInfo(CloudDBZoneQuery<SportInfo> query) {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
        }

        Task<CloudDBZoneSnapshot<SportInfo>> queryTask = mCloudDBZone.executeQuery(query,
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
        queryTask.addOnSuccessListener(new OnSuccessListener<CloudDBZoneSnapshot<SportInfo>>() {
            @Override
            public void onSuccess(CloudDBZoneSnapshot<SportInfo> snapshot) {
                try {
                    processQueryResult(snapshot);
                } catch (AGConnectCloudDBException e) {
                    Log.e(TAG, "onfailed: "+e.getErrMsg() );
                }
                Log.i(TAG, "onSuccess: query result success");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.i(TAG, "onSuccess: query result failed," + e.toString());
            }
        });

    }


    private void processQueryResult(CloudDBZoneSnapshot<SportInfo> snapshot) throws AGConnectCloudDBException {
        CloudDBZoneObjectList<SportInfo> sportInfoCursor = snapshot.getSnapshotObjects();

        try {
            int index = 1;
            mSportList.clear();
            while (sportInfoCursor.hasNext()) {
                SportInfo info = sportInfoCursor.next();
                SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                Log.d(TAG, "getTime=" + info.getTime());
                mSportList.add(new SportAdapter.SportItem(info.getId(),info.getName(),info.getAddr(),info.getTime(),info.getCount().toString(),info.getRequest().toString(), info.getUserlist().toString()));

            }
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "processQueryResult: " + e.getMessage());
        } finally {
            snapshot.release();
//            mCloudDB.closeCloudDBZone(mCloudDBZone);

            Collections.sort(mSportList);



            mSportAdapter.notifyDataSetChanged();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mCloudDBZone != null) {
            updateData();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();


    }

    @Override
    protected void onDestroy() {
        try {
            mCloudDB.closeCloudDBZone(mCloudDBZone);
        } catch (AGConnectCloudDBException e) {
            e.printStackTrace();
        }

        if (AGConnectAuth.getInstance().getCurrentUser() != null) {
            // signOut
            AGConnectAuth.getInstance().signOut();
        }

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_add:
                startActivity(new Intent(MainActivity.this, PostActivity.class));
                break;
        }
    }


}

