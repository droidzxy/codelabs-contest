package com.example.playtogether;

import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class SportAdapter extends RecyclerView.Adapter<SportAdapter.ViewHolder> {
    private static final String TAG = SportAdapter.class.getSimpleName();

    public static class SportItem implements Comparable<SportItem>{

        public String id;
        public String name;
        public String addr;
        public String time;
        public String count;
        public String request;
        public String userlist;

        public SportItem() {
        }

        public SportItem(String id, String name, String addr, String time, String count, String request, String userlist) {

            this.id = id;
            this.name = name;
            this.addr = addr;
            this.time = time;
            this.count = count;
            this.request = request;
            this.userlist = userlist;
        }

        @Override
        public String toString() {
            return id + ","+ name;
        }

        // sort From big to small
        @Override
        public int compareTo(SportItem o) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            Date dateA = new Date();
            Date dateB = new Date();
            try {
                dateA = sdf.parse( this.time);
                dateB = sdf.parse( o.time);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(dateA.before(dateB)) {
                return 1;
            } else if(dateA.after(dateB)) {
                return -1;
            } else {
                return 0;
            }
        }
    }


    private final ArrayList<SportItem> mList;
    private MainActivity.UiCallBack mCallBack;

    public SportAdapter(ArrayList<SportItem> list, MainActivity.UiCallBack callBack) {
        mList = list;
        mCallBack = callBack;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "time=" + mList.get(position).time);
        holder.txtSport.setText(String.valueOf(mList.get(position).name));
        holder.txtAddr.setText(String.valueOf(mList.get(position).addr));
        holder.txtTime.setText(String.valueOf(mList.get(position).time).substring(5));
        holder.txtCount.setText(String.valueOf(mList.get(position).request));

        int pos = position;

        String userlist = mList.get(position).userlist;
        if(userlist.contains(GlobalInfo.gCurrentUser)) {
            holder.txtSport.getPaint().setFakeBoldText(true);
            holder.txtAddr.getPaint().setFakeBoldText(true);
            holder.txtTime.getPaint().setFakeBoldText(true);
            holder.txtCount.getPaint().setFakeBoldText(true);
        } else {
            holder.txtSport.getPaint().setFakeBoldText(false);
            holder.txtAddr.getPaint().setFakeBoldText(false);
            holder.txtTime.getPaint().setFakeBoldText(false);
            holder.txtCount.getPaint().setFakeBoldText(false);
        }



        holder.layoutItem.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
//                memberInfo info = new memberInfo();
//                info.setId(mList.get(pos).id);
//                info.setName(String.valueOf(mList.get(pos).name));
//                info.setLevel(mList.get(pos).level);
//                info.setPoints(mList.get(pos).points);
//
//                mCallBack.onDelete(info);
                SportInfo info = new SportInfo();
                info.setId(mList.get(pos).id);
                info.setName(mList.get(pos).name);
                info.setAddr(mList.get(pos).addr);
                info.setTime(mList.get(pos).time);
                info.setCount(Integer.parseInt(mList.get(pos).count));
                info.setRequest(Integer.parseInt(mList.get(pos).request));
                info.setUserlist(mList.get(pos).userlist);

                GlobalInfo.gInfo = info;

                view.getContext().startActivity(new Intent(view.getContext(), DetailActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final LinearLayout layoutItem;
        public final TextView txtSport;
        public final TextView txtAddr;
        public final TextView txtTime;
        public final TextView txtCount;

        public ViewHolder(View view) {
            super(view);
            layoutItem = (LinearLayout)view.findViewById(R.id.layout_item);
            txtSport = (TextView) view.findViewById(R.id.txt_item_sport);
            txtAddr = (TextView) view.findViewById(R.id.txt_item_addr);
            txtTime = (TextView) view.findViewById(R.id.txt_item_time);
            txtCount = (TextView) view.findViewById(R.id.txt_item_count);
        }

    }
}
