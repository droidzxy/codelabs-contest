package com.example.playtogether;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.huawei.agconnect.cloud.database.CloudDBZoneObjectList;
import com.huawei.agconnect.cloud.database.CloudDBZoneQuery;
import com.huawei.agconnect.cloud.database.CloudDBZoneSnapshot;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PostActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = PostActivity.class.getSimpleName();

    private ImageView imgBack;
    private EditText edtDate;
    private EditText edtTime;
    private EditText edtName;
    private EditText edtAddr;
    private EditText edtCount;
    private TextView txtUserlistLabel;
    private TextView txtUserlist;

    private Button btnPost;
    private Button btnRePost;
    private Button btnCancel;

    public PostActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.statuscolor));
        }

        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(this);


        edtDate = findViewById(R.id.edt_post_date);
        edtDate.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    showDatePickDlg();
                    return true;
                }
                return false;
            }
        });
        edtTime = findViewById(R.id.edt_post_time);
        edtTime.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    showTimePickDlg();
                    return true;
                }
                return false;
            }
        });

        btnRePost = findViewById(R.id.btn_repost);
        btnRePost.setOnClickListener(this);
        btnPost = findViewById(R.id.btn_post);
        btnPost.setOnClickListener(this);
        btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);

        edtName = findViewById(R.id.edt_post_name);
        edtAddr = findViewById(R.id.edt_post_addr);
        edtDate = findViewById(R.id.edt_post_date);
        edtTime = findViewById(R.id.edt_post_time);
        edtCount = findViewById(R.id.edt_post_count);
        txtUserlistLabel = findViewById(R.id.txt_post_userlist);
        txtUserlist = findViewById(R.id.edt_post_userlist);



    }

    @Override
    protected void onStart() {
        super.onStart();

        initUI();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initUI() {


        CloudDBZoneQuery<SportInfo> query = CloudDBZoneQuery.where(SportInfo.class).equalTo("id", GlobalInfo.gCurrentUser);

        Task<CloudDBZoneSnapshot<SportInfo>> queryTask = GlobalInfo.gCloudDBZone.executeQuery(query,
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
        queryTask.addOnSuccessListener(new OnSuccessListener<CloudDBZoneSnapshot<SportInfo>>() {
            @Override
            public void onSuccess(CloudDBZoneSnapshot<SportInfo> snapshot) {
//                processQueryResult(snapshot);
                CloudDBZoneObjectList<SportInfo> sportInfoCursor = snapshot.getSnapshotObjects();
                try {
                    if(sportInfoCursor.size() > 0) {
                        SportInfo info = sportInfoCursor.get(0);
//
                        btnRePost.setVisibility(View.VISIBLE);
                        btnPost.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                        edtName.setEnabled(false);
                        edtAddr.setEnabled(false);
                        edtDate.setEnabled(false);
                        edtTime.setEnabled(false);
                        edtCount.setEnabled(false);
                        txtUserlistLabel.setVisibility(View.VISIBLE);
                        txtUserlist.setVisibility(View.VISIBLE);

                        edtName.setText(info.getName());
                        edtAddr.setText(info.getAddr());
                        edtDate.setText(info.getTime().substring(0, 10));
                        edtTime.setText(info.getTime().substring(11));
                        edtCount.setText(info.getCount().toString());

                        String[] users = info.getUserlist().toString().split(";");
                        String list = "";
                        for(int i = 0; i < users.length; i++) {
                            if(list.length() > 0) {
                                list += "\n";
                            }
                            list += users[i];
                        }
                        txtUserlist.setText(list);
                    }
                    else {
                        btnRePost.setVisibility(View.GONE);
                        btnPost.setVisibility(View.VISIBLE);
                        btnCancel.setVisibility(View.VISIBLE);
                        edtName.setEnabled(true);
                        edtAddr.setEnabled(true);
                        edtDate.setEnabled(true);
                        edtTime.setEnabled(true);
                        edtCount.setEnabled(true);
                        txtUserlistLabel.setVisibility(View.GONE);
                        txtUserlist.setVisibility(View.GONE);

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy/MM/dd");
                        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");

                        edtDate.setText(formatDate.format(calendar.getTime()));
                        edtTime.setText(formatTime.format(calendar.getTime()));
                        edtName.setText("");
                        edtAddr.setText("");
                        edtCount.setText("2");
                    }


//

                } catch (AGConnectCloudDBException e) {
                    Log.w(TAG, "processQueryResult: " + e.getMessage());
                } finally {
                    snapshot.release();
                }

                Log.i(TAG, "onSuccess: query result success");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.i(TAG, "onSuccess: query result failed," + e.toString());
            }
        });








    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_add:

                break;
            case R.id.btn_repost:
                btnRePost.setVisibility(View.GONE);
                btnPost.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);

                edtName.setEnabled(true);
                edtAddr.setEnabled(true);
                edtDate.setEnabled(true);
                edtTime.setEnabled(true);
                edtCount.setEnabled(true);
                txtUserlistLabel.setVisibility(View.GONE);
                txtUserlist.setVisibility(View.GONE);
                break;
            case R.id.btn_post:
                if(edtName.getText().toString().trim().length() > 0 && edtAddr.getText().toString().trim().length() > 0 && edtCount.getText().toString().trim().length() > 0) {
                    postSport();
                } else {
                    Toast.makeText(PostActivity.this, "输入内容不能为空！", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_cancel:
                confirmBeformCancel();
                break;
        }
    }

    private void confirmBeformCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PostActivity.this);
        builder.setTitle("确认");
        builder.setMessage("您确定要删除当前活动吗？");
        builder.setCancelable(true);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelSport();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void postSport() {

        if (GlobalInfo.gCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }
        SportInfo info = new SportInfo();
        info.setId(GlobalInfo.gCurrentUser);
        info.setName(edtName.getText().toString().trim());
        info.setAddr(edtAddr.getText().toString().trim());
        info.setTime(edtDate.getText().toString().trim() + " " + edtTime.getText().toString().trim().substring(0, 5));
        info.setCount(Integer.parseInt(edtCount.getText().toString().trim()));
        info.setUserlist(GlobalInfo.gCurrentUser);

        info.setRequest(1);


        Task<Integer> upsertTask = GlobalInfo.gCloudDBZone.executeUpsert(info);
        upsertTask.addOnSuccessListener(new OnSuccessListener<Integer>() {
            @Override
            public void onSuccess(Integer cloudDBZoneResult) {
                Log.i(TAG, "Upsert " + cloudDBZoneResult + " records");
                Toast.makeText(PostActivity.this, "发布成功！", Toast.LENGTH_SHORT).show();

                new CountDownTimer(1000, 100) {
                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        onBackPressed();
                    }
                }.start();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "zzz, failed add," + e.toString());
            }
        });
    }

    private void cancelSport() {
        SportInfo info = new SportInfo();
        info.setId(GlobalInfo.gCurrentUser);

        Task<Integer> deleteTask = GlobalInfo.gCloudDBZone.executeDelete(info);
        deleteTask.addOnSuccessListener(new OnSuccessListener<Integer>() {
            @Override
            public void onSuccess(Integer integer) {
//                mUiCallBack.onDelete(bookInfoList);
                Toast.makeText(PostActivity.this, "删除成功！", Toast.LENGTH_SHORT).show();

                new CountDownTimer(1000, 100) {
                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        onBackPressed();
                    }
                }.start();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
//                mUiCallBack.updateUiOnError("Delete book info failed");
                Toast.makeText(PostActivity.this, "删除失败！", Toast.LENGTH_SHORT).show();

                Log.e(TAG, "delete error " + e.toString());
            }
        });
    }

    protected void showDatePickDlg() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                edtDate.setText(year + "/" + String.format("%02d", (monthOfYear + 1)) + "/" + String.format("%02d", dayOfMonth));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    protected void showTimePickDlg() {
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                edtTime.setText(String.format("%02d", i) + ":" +  String.format("%02d", i1));
            }

        }, 0, 0, true);

        timePickerDialog.show();
    }

}