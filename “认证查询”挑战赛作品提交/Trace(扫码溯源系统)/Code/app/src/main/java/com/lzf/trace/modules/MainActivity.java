package com.lzf.trace.modules;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectUser;
import com.lzf.trace.IBaseActivity;
import com.lzf.trace.R;
import com.lzf.trace.modules.manage.avtivity.LoginActivity;
import com.lzf.trace.modules.manage.avtivity.ProductListActivity;
import com.lzf.trace.modules.scan.avtivity.ResultActivity;
import com.lzf.trace.publics.utils.T;
import com.qmuiteam.qmui.widget.QMUITopBar;

import com.king.zxing.CaptureActivity;
import com.king.zxing.CameraScan;

public class MainActivity extends IBaseActivity {

    private QMUITopBar mTopbar;
    private LinearLayout topscanbtn, manage_ll;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initDatas();
        initViews();
    }


    @Override
    public void initViews() {
        mTopbar = findViewById(R.id.topbar);
        mTopbar.setTitle("扫码溯源系统");
//        mTopbar.addRightTextButton("管理", R.id.topbar_right_about_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//        mTopbar.setBackground();

        topscanbtn = findViewById(R.id.topscanbtn);
        manage_ll = findViewById(R.id.manage_ll);
        topscanbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                测试用
//                Intent intent = new Intent(mContext, ResultActivity.class);
//                intent.putExtra("code","4515656422155");
//                startActivity(intent);
                //跳转的默认扫码界面
                startActivityForResult(new Intent(mContext, CaptureActivity.class), 111);
            }
        });

        manage_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AGConnectUser user = AGConnectAuth.getInstance().getCurrentUser();
                if (user != null) {
                    T.showShort(mContext, "已有登录用户，" + user.getPhone());
                    startActivity(new Intent(mContext, ProductListActivity.class));
                } else {
                    startActivity(new Intent(mContext, LoginActivity.class));
                }
            }
        });

    }

    @Override
    public void initDatas() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == RESULT_OK) {
            String result = CameraScan.parseScanResult(data);
//            T.showShort(mContext, result);
            Intent intent = new Intent(mContext, ResultActivity.class);
            intent.putExtra("code", result);
            startActivity(intent);
        }
    }
}