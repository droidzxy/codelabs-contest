package com.lzf.trace.modules.manage.avtivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huawei.agconnect.api.AGConnectApi;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.auth.AGConnectUser;
import com.huawei.agconnect.auth.PhoneAuthProvider;
import com.huawei.agconnect.auth.PhoneUser;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.agconnect.auth.VerifyCodeResult;
import com.huawei.agconnect.auth.VerifyCodeSettings;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hmf.tasks.TaskExecutors;
import com.lzf.trace.IBaseActivity;
import com.lzf.trace.R;
import com.lzf.trace.publics.utils.EmptyUtils;
import com.lzf.trace.publics.utils.T;
import com.qmuiteam.qmui.widget.QMUITopBar;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends IBaseActivity {

    private static String countryCodeStr = "86";

    EditText mm_login_phone_et;
    EditText phone_code_et;
    TextView get_code_tv;
    Button login_btn;
    private QMUITopBar mTopbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
        initDatas();
    }


    @Override
    public void initViews() {
        mTopbar = findViewById(R.id.topbar);
        mm_login_phone_et = findViewById(R.id.mm_login_phone_et);
        phone_code_et = findViewById(R.id.phone_code_et);
        get_code_tv = findViewById(R.id.get_code_tv);
        login_btn = findViewById(R.id.login_btn);
        mTopbar.setTitle("管理员登录");
        mTopbar.addLeftBackImageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        get_code_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (EmptyUtils.isEmpty(mm_login_phone_et.getText().toString().trim())) {
                    T.showShort(mContext, "手机号不能为空！");
                    return;
                }
                VerifyCodeSettings settings = new VerifyCodeSettings.Builder()
                        .action(VerifyCodeSettings.ACTION_REGISTER_LOGIN)
                        .sendInterval(30)
                        .locale(Locale.CHINA)
                        .build();
                Task<VerifyCodeResult> task = AGConnectAuth.getInstance().requestVerifyCode(countryCodeStr, mm_login_phone_et.getText().toString(), settings);
                task.addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<VerifyCodeResult>() {
                    @Override
                    public void onSuccess(VerifyCodeResult verifyCodeResult) {
                        //验证码申请成功
                        T.showShort(mContext, "验证码发送成功！");
                        get_code_tv.setText("重新获取");
                    }
                }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        T.showShort(mContext, "验证码发送失败，" + e.getMessage());
                    }
                });
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (EmptyUtils.isEmpty(mm_login_phone_et.getText().toString().trim())) {
                    T.showShort(mContext, "手机号不能为空！");
                    return;
                }
                if (EmptyUtils.isEmpty(phone_code_et.getText().toString().trim())) {
                    T.showShort(mContext, "验证码不能为空！");
                    return;
                }
//                创建手机用户
//                PhoneUser phoneUser = new PhoneUser.Builder()
//                        .setCountryCode(countryCodeStr)
//                        .setPhoneNumber(mm_login_phone_et.getText().toString())
//                        .setVerifyCode(phone_code_et.getText().toString())
//                        .build();
//                AGConnectAuth.getInstance().createUser(phoneUser)
//                        .addOnSuccessListener(new OnSuccessListener<SignInResult>() {
//                            @Override
//                            public void onSuccess(SignInResult signInResult) {
//                                //创建帐号成功后，默认已登录
//                                T.showShort(mContext, "登录成功！");
//                            }
//                        })
//                        .addOnFailureListener(new OnFailureListener() {
//                            @Override
//                            public void onFailure(Exception e) {
//                                T.showShort(mContext, "登录失败，" + e.getMessage());
//                            }
//                        });
//                手机登录
                AGConnectAuthCredential credential = PhoneAuthProvider.credentialWithVerifyCode(
                        countryCodeStr,
                        mm_login_phone_et.getText().toString(), null, phone_code_et.getText().toString());
                AGConnectAuth.getInstance().signIn(credential)
                        .addOnSuccessListener(new OnSuccessListener<SignInResult>() {
                            @Override
                            public void onSuccess(SignInResult signInResult) {
                                //获取登录信息
                                T.showShort(mContext, "登录成功！");
                                startActivity(new Intent(mContext, ProductListActivity.class));
                                finish();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {
                                T.showShort(mContext, "登录失败，" + e.getMessage());
                            }
                        });
            }
        });

    }

    @Override
    public void initDatas() {
        AGConnectUser user = AGConnectAuth.getInstance().getCurrentUser();
        if (user != null) {
            T.showShort(mContext, "已有登录用户，" + user.getPhone());
            startActivity(new Intent(mContext, ProductListActivity.class));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AGConnectApi.getInstance().activityLifecycle().onActivityResult(requestCode, resultCode, data);
    }

}