package com.lzf.trace.publics.common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Constants {
    /**
     * 数据格式
     **/
    public static String CodeType = "utf-8";
    public static String ServletSuccessCode = "1";
    public static String ServletFailCode = "2";

    public static final String NetError = "网络请求出错";
    public static final String ImageNetError = "图片上传失败";

    public static final String NetSuccessTag = "0";
    public static final String NetErrorTag = "1";

    public static final String RefreshSuccess = "加载成功";
    public static final String RefreshFail = "加载失败";

    public static final String UserNotLogin = "用户未登录，请登录";

    public static final int PAGE_SIZE = 10;

    public static final String DB_NAME = "test";

    // 首次登录
    public static final String FirstLoginFlag = "FIRST_LOGIN_FLAG";
    public static final String USER_INFO_SHARE = "userInfo";
    public static final String FONT_SIZE_SHARE = "fontSize";
    public static final String SYSTEM_SETTING_SHARE = "systemSetting";
    // 用户id
    public static final String USER_ID = "id";
    // 用户名
    public static final String USER_NAME = "userName";
    // 用户手机号
    public static final String USER_PHONE = "phoneNum";
    // 用户头像
    public static final String PHOTO_URL = "photoUrl";
    // 内部外用户标识（1：外部用户2：内部用户）
    public static final String USER_CLASSIFY = "userClassify";
    // 用户性别
    public static final String SEX = "sex";
    // 是否设置过用户名（0：未设置；1：已设置）
    public static final String ISSETUSERNAME = "isSetUserName";
    // 用户token
    public static final String TOKEN = "token";
    // 用户邮箱
    public static final String EMAIL = "emailAddress";
    // 用户部门
    public static final String DEPARTMENT = "departmentName";
    // 用户职级
    public static final String GRADE = "grade";
    //字体大小
    public static final String FONTSIZE = "fontsize";


    public static final String SERVICE_CONFIG = "servicrConfig";
    public static final String SERVICE_IP = "servicrIp";
    public static final String SERVICE_PORT = "servicrPort";
//    public static final String SERVICE_IP_DEFAULT = "jnzdgc.test.zhongyuankeji.cn";
    public static final String SERVICE_IP_DEFAULT = "www.baidu.com";
    public static final String SERVICE_PORT_DEFAULT = "80";
//    public static final String SERVICE_IP_DEFAULT = "duhouya.tunnel.zhongyuankeji.cn";
//    public static final String SERVICE_PORT_DEFAULT = "80";


    public static final String JINGDU = "jingdu";
    public static final String WEIDU = "weidu";
//    public static final String SERVICE_IP_DEFAULT= "jnccjc.test.zhongyuankeji.cn";
//    public static final String SERVICE_PORT_DEFAULT = "80";

    /**
     * 头像名称
     **/
    public static final String PHOTO_FILE_NAME = new SimpleDateFormat(
            "yyyyMMddHHmmss").format(new Date()) + ".jpg";

    /**
     * @description 接口定义
     */
    public static class ServiceInterFace {
        public static String BaseUrl = "http://139.129.666.666/ppp";
//        public static String BaseUrl = "";
//        public static String BaseUrl = "http://192.168.1.106/jncc_web";
//      public static String BaseUrl = "http://192.168.1.114/jncc_web";

        public static final String GetHomeNewsUrl = BaseUrl + "/api/home/getHomeNewsList";

    }

}
