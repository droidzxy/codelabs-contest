package com.lzf.trace.modules.manage.dto;

public class AddEvent {
//    是否新增
    private Boolean success;

    private Product mProduct;


    public AddEvent() {
    }

    public AddEvent(Boolean success) {
        this.success = success;
    }

    public AddEvent(Boolean success, Product mProduct) {
        this.success = success;
        this.mProduct = mProduct;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Product getmProduct() {
        return mProduct;
    }

    public void setmProduct(Product mProduct) {
        this.mProduct = mProduct;
    }
}
