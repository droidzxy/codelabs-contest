import { agconnect } from "./store";


async function getUser() {
    return agconnect.auth().getCurrentUser()
}
async function signInAnonymously() {
    return agconnect.auth().signInAnonymously()
}
async function requestPhoneVerifyCode(phoneNumber, countryCode = '86', sendInterval = 5) {
    return agconnect
        .auth()
        .requestPhoneVerifyCode(countryCode,
            phoneNumber,
            agconnect.auth.Action.ACTION_REGISTER_LOGIN,
            'zh_CN',
            sendInterval)
}
async function signInByPhone(phoneNumber, verifyCode, password = '', countryCode = '86') {
    let credential = agconnect.auth.PhoneAuthProvider.credentialWithVerifyCode(countryCode, phoneNumber, password, verifyCode);
    if (!credential) throw 'credential fail'
    return agconnect.auth().signIn(credential)
}
async function logout() {
    return agconnect.auth().signOut()
}
export {
    getUser
    , signInAnonymously
    , requestPhoneVerifyCode
    , signInByPhone
    , logout
}