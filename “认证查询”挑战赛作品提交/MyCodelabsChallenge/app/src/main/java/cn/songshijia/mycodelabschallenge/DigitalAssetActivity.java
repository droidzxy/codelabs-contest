package cn.songshijia.mycodelabschallenge;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

public class DigitalAssetActivity extends AppCompatActivity {
    private Intent intent = new Intent();
    private ImageView imageView;
    private EditText editText;
    private TextView textView;
    private String type = "picture";

    //选取图片
    private final ActivityResultLauncher<String> launcher = registerForActivityResult(
            new ActivityResultContracts.GetContent(), result -> {
                //result为选取的图片的Uri
                System.out.println(result);
                intent.putExtra("uri", result.toString());
                switch (type) {
                    case "picture":
                        imageView.setImageURI(result);
                        imageView.setVisibility(View.VISIBLE);
                        break;
                    case "audio":
                    case "file":
                        textView.setText(result.getPath());
                        textView.setVisibility(View.VISIBLE);
                        break;
                    default:
                        System.out.println("invalid type");
                }
            }
    );

    //调用相册选择图片
    protected void mLaunch(String type) {
        launcher.launch(type);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_asset);
        EditText name = findViewById(R.id.set_name);
        imageView = findViewById(R.id.image_view);
        editText = findViewById(R.id.enter_text);
        textView = findViewById(R.id.show_uri);
        findViewById(R.id.choose_content).setOnClickListener(v -> {
            switch (type) {
                case "picture":
                    mLaunch("image/*");
                    break;
                case "audio":
                    mLaunch("audio/*");
                    break;
                case "file":
                    mLaunch("*/*");
                    break;
                case "text":
                    editText.setVisibility(View.VISIBLE);
                    break;
                default:
                    System.out.println("invalid type");
            }
        });
        findViewById(R.id.ok).setOnClickListener(v -> {
            intent.putExtra("name", name.getText().toString());
            intent.putExtra("type", type);
            setResult(RESULT_OK, intent);
            if ("text".equals(type)) {
                intent.putExtra("text", editText.getText().toString());
            }
            finish();
        });

        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        //第一种获得单选按钮值的方法
        //为radioGroup设置一个监听器:setOnCheckedChanged()
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            editText.setVisibility(View.INVISIBLE);
            imageView.setVisibility(View.INVISIBLE);
            textView.setVisibility(View.INVISIBLE);
            RadioButton radioButton = findViewById(checkedId);
            type = radioButton.getText().toString();
        });
    }
}