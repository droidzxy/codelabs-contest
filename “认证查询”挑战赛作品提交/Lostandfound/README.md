### **项目简介**

本项目为Android以及集成认证服务、云数据库服务、云存储服务开发的一个简易版的失物招领App，目前已完成的功能有

1、可通过手机、邮箱、华为帐号以及匿名四种方式进行登录

2、可通过手机、邮箱两种方式进行注册

3、可对云数据库进行增删改查操作

4、更改用户头像、昵称、手机号、邮箱、密码功能

5、关联、解除关联功能

6、重置密码、删除用户、退出帐号功能

### **界面截图**

### <img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430182907.13816907868619484858717522595783:50530501040440:2800:BDD6D0FBC727B8FFC76B2FA6112B848AC6462DC1EAE887592FD86827915DD1C1.jpg" alt="img" style="zoom:50%;" />            <img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430182907.82767863954375111874423380470198:50530501040440:2800:7FA623B9241E446248D648C1EB521342D0135DE4E475106BFE842785070E95E3.jpg" alt="img" style="zoom:50%;" />

<img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430182907.51729303743541249733662338958191:50530501040440:2800:15603001BA6B01C5760F6489B587999AE02D8CB8AA72D34813A072135C8A5E04.jpg" alt="img" style="zoom:50%;" />            <img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430182907.49506370434338294254960853264352:50530501040440:2800:12458E15B777ABCE3AD58DA54451C8076E881E7FDBC9D9552A791A335502F869.jpg" alt="Screenshot_20220430_182715_com.haoc.lostandfound.jpg" style="zoom:50%;" />

<img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430182907.00009737423498526651487869722409:50530501040440:2800:C4E192D5D476205635ADF3AA85EBF8299E2F23A263B8FC4C2EE18C7EE857DB13.jpg" alt="Screenshot_20220430_182622_com.haoc.lostandfound.jpg" style="zoom:50%;" />            <img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430182907.80131802741051484853621814156608:50530501040440:2800:65FF6483720EF3C1D5F7E41FD6236965AE66AFD2E06B6A969D7C4EF06B02365D.jpg" alt="img" style="zoom:50%;" />

### **演示效果**

1、使用手机号进行登录![cke_198246.gif](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430164531.82712945197664719971413605638022:50530501040440:2800:2CCB38E16D8461B084DF09C65CA2A1C3047E4D0529AC9578D7286098E321A09E.gif)

2、使用华为帐号进行登录

![cke_90414.gif](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430163817.28493098047160334336748052078463:50530501040440:2800:04D9E228E4765A201A24568B23B4823357E1F8406F7F3C6C679A0DFE4C818F8D.gif)

3、新增失物

![petal_20220430_160617.gif](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430163305.79267717898579928774258977372664:50530501040440:2800:DB5A27E06CA656187C3E7D07869ABAE2B6FD189897E7874C2A2740345613633C.gif)

4、查找失物

![img](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430163305.05382131524623105959456285611995:50530501040440:2800:3F6DE56FA4EA966C3B2B6EEE1009EBCA2612F8374B305ACF1D89B7BE4E5340CB.gif)

5、删除失物

![HwVideoEditor_2022_04_30_160114437.gif](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430163305.27102116811655729282453555109078:50530501040440:2800:C8DC353DB301F06AD74E0124440AF8115A9B6AD0980025D7427922DC04FB35F3.gif)

6、更新头像

![petal_20220430_161325.gif](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220430163305.41133080206932432990176265735594:50530501040440:2800:472160B92F0129A96D345079DE464C50E99ADD9A106621E449E9F95A38669AEE.gif)

### **云数据库配置**

![cke_3813.png](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220504215119.06948892393497171892145036753277:50530508035055:2800:CC6F7D225B6B2A01C9CBCE6166E4DD99B2BCE8EEBC48ADE22EED43DF93969DFC.png)

![cke_50038.png](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220504215204.55137485969373295146589248589645:50530508035055:2800:16D6DFA18AF890D64C477EA91F822958CBD38CF1B6AE8C6E5EC032DCB969247D.png)

![cke_58011.png](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220504215214.26930164553520023830469211003015:50530508035055:2800:C1FE70BEC9A03697B679E7F388B4862246E54A01CCCC9D423B1BB178DCE43D97.png)

### **关键代码**

**1、登录**

这里以邮箱登录为例，手机号同理，获取用户填写的邮箱、密码以及验证码，进行判断后调用相应接口创建Email帐号凭证，然后将凭证传给signIn函数进行登录处理

```java
String email = accountEdit.getText().toString().trim();
String password = passwordEdit.getText().toString().trim();
String verifyCode = verifyCodeEdit.getText().toString().trim();
AGConnectAuthCredential credential;
if (TextUtils.isEmpty(verifyCode)) {
    credential = EmailAuthProvider.credentialWithPassword(email, password);
} else {
    //如果您没有密码，密码参数可以为空
    credential = EmailAuthProvider.credentialWithVerifyCode(email, password, verifyCode);
}
signIn(credential);
```

```java
private void signIn(AGConnectAuthCredential credential) {
    AGConnectAuth.getInstance().signIn(credential)
            .addOnSuccessListener(new OnSuccessListener<SignInResult>() {
                @Override
                public void onSuccess(SignInResult signInResult) {
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(LoginActivity.this, "登录失败:" + e, Toast.LENGTH_LONG).show();
                }
            });
}
```

**2、新增失物**

将通过Intent传过来的数据进行解析并添加到ThingInfo对象

```
private void processAddAction(Intent data) {
    ThingInfo ThingInfo = new ThingInfo();
    int thingID = data.getIntExtra(ThingEditFields.Thing_ID, -1);
    if (thingID == -1) {
        ThingInfo.setId(mCloudDBZoneWrapper.getThingIndex() + 1);
    } else {
        ThingInfo.setId(thingID);
    }
    ThingInfo.setThingName(data.getStringExtra(ThingEditFields.Thing_NAME));
    ThingInfo.setLocation(data.getStringExtra(ThingEditFields.LOCATION));
    ThingInfo.setPhone(data.getStringExtra(ThingEditFields.PHONE));
    String dateTime = data.getStringExtra(ThingEditFields.Time);
    ThingInfo.setTime(DateUtils.parseDate(dateTime));
    toggleShowAllState(false);
    mHandler.post(() -> mCloudDBZoneWrapper.upsertThingInfos(ThingInfo));
}
```

通过executeUpsert()将ThingInfo对象写入到Cloud DB zone中

```
public void upsertThingInfos(ThingInfo ThingInfo) {
    if (mCloudDBZone == null) {
        Log.w(TAG, "CloudDBZone is null, try re-open it");
        return;
    }

    Task<Integer> upsertTask = mCloudDBZone.executeUpsert(ThingInfo);
    upsertTask.addOnSuccessListener(new OnSuccessListener<Integer>() {
        @Override
        public void onSuccess(Integer cloudDBZoneResult) {
            Log.i(TAG, "Upsert " + cloudDBZoneResult + " records");
        }
    }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(Exception e) {
            mUiCallBack.updateUiOnError("Insert thing info failed");
        }
    });
}
```

**2、搜索失物**

```
private void processSearchAction(Intent data) {
    CloudDBZoneQuery<ThingInfo> query = CloudDBZoneQuery.where(ThingInfo.class);
    String thingName = data.getStringExtra(ThingEditFields.Thing_NAME);
    if (!TextUtils.isEmpty(thingName)) {
        // 查询thingName包含特定字符的结果
        query.contains(ThingEditFields.Thing_NAME, thingName);
        mQueryInfo.thingName = thingName;
    }

    int showCount = data.getIntExtra(ThingEditFields.SHOW_COUNT, 0);
    if (showCount > 0) {
        query.limit(showCount);
        mQueryInfo.showCount = showCount;
    }

    toggleShowAllState(true);
    mHandler.post(() -> mCloudDBZoneWrapper.queryThings(query));
}
```

```
public void queryThings(CloudDBZoneQuery<ThingInfo> query) {
    if (mCloudDBZone == null) {
        Log.w(TAG, "CloudDBZone is null, try re-open it");
        return;
    }

    Task<CloudDBZoneSnapshot<ThingInfo>> queryTask = mCloudDBZone.executeQuery(query,
            CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
    queryTask.addOnSuccessListener(new OnSuccessListener<CloudDBZoneSnapshot<ThingInfo>>() {
        @Override
        public void onSuccess(CloudDBZoneSnapshot<ThingInfo> snapshot) {
            processQueryResult(snapshot);
        }
    }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(Exception e) {
            mUiCallBack.updateUiOnError("Query failed");
        }
    });
}
```

### **参考资料**

1、[使用入门-Android-认证服务 (huawei.com)](https://developer.huawei.com/consumer/cn/doc/development/AppGallery-connect-Guides/agc-auth-android-getstarted-0000001053053922)

2、[使用入门-Android-云数据库 (huawei.com)](https://developer.huawei.com/consumer/cn/doc/development/AppGallery-connect-Guides/agc-clouddb-get-started-0000001127676473)

3、[使用入门-Android-云存储 (huawei.com)](https://developer.huawei.com/consumer/cn/doc/development/AppGallery-connect-Guides/agc-cloudstorage-getstarted-android-0000001055406164)

4、[认证服务（高级） (huawei.com)](https://developer.huawei.com/consumer/cn/codelabsPortal/carddetails/AuthenticationService-hard)

5、[云数据库（高级） (huawei.com)](https://developer.huawei.com/consumer/cn/codelabsPortal/carddetails/CloudDB-Android-Hard)

6、[云存储（Android-高级） (huawei.com)](https://developer.huawei.com/consumer/cn/codelabsPortal/carddetails/CloudStorage-Android-Hard)

7、[Overview-com.huawei.agconnect.auth-Android-认证服务](https://developer.huawei.com/consumer/cn/doc/development/AppGallery-connect-References/agc-auth-service-api-overview-0000001054403973)

8、[Overview-com.huawei.agconnect.cloud.database-Android-云数据库](https://developer.huawei.com/consumer/cn/doc/development/AppGallery-connect-References/clouddb-0000001127834753)

9、[Overview-com.huawei.agconnect.cloud.storage-Android-云存储](https://developer.huawei.com/consumer/cn/doc/development/AppGallery-connect-References/overview-0000001055088626)

### **结语**

项目开发用了10多天的时间，主要是在官方两个demo的基础上进行更改并添加进自己的需求，在这个过程中收获真的很多，印象比较深刻的是更新头像那一块，遇到问题后问官方人员、同学，改bug改到凌晨，最后看着App一点一点变成自己想要的模样，也是非常开心。对于这个项目后续我还有很多想法，比如可以在用户重置密码后发送短信通知用户、根据返回错误码给出不同的中文提示等等，可能会进行不定期更新。在文章中也没有完全地展示项目，有兴趣的小伙伴也可以在AGC控制台上创建一个项目，下载源码后进行二次开发，转载请注明出处，喜欢的话给我的项目点个Star吧~
