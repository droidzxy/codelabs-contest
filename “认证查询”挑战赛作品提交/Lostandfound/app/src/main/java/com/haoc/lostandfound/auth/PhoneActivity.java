package com.haoc.lostandfound.auth;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.haoc.lostandfound.R;
import com.haoc.lostandfound.utils.Myutils;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.auth.PhoneAuthProvider;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.agconnect.auth.VerifyCodeSettings;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.TaskExecutors;

public class PhoneActivity extends Activity {
    private EditText accountEdit;
    private EditText verifyCodeEdit;
    Button send;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_link);
        accountEdit = findViewById(R.id.et_account);
        verifyCodeEdit = findViewById(R.id.et_verify_code);
        send = findViewById(R.id.btn_send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendVerificationCode();
            }
        });

        findViewById(R.id.btn_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = accountEdit.getText().toString().trim();
                String verifyCode = verifyCodeEdit.getText().toString().trim();
                /*
                通过手机号和验证码获取凭证。
                如果verifyCode为空，此函数会调用credentialWithPassword接口。
                Link操作时，verifyCode必填。
                 */
                AGConnectAuthCredential credential = PhoneAuthProvider.credentialWithVerifyCode(
                        "86",
                        phoneNumber,
                        null,
                        verifyCode);
                if (AGConnectAuth.getInstance().getCurrentUser() != null) {
                    AGConnectAuth.getInstance().getCurrentUser().link(credential)
                            .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<SignInResult>() {
                                @Override
                                public void onSuccess(SignInResult signInResult) {
                                    Toast.makeText(PhoneActivity.this, "关联手机号成功", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                                @Override
                                public void onFailure(Exception e) {
                                    Toast.makeText(PhoneActivity.this, "关联手机号失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                }
            }
        });

        findViewById(R.id.btn_unlink).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AGConnectAuth.getInstance().getCurrentUser() != null) {
                    // 解除手机号关联
                    AGConnectAuth.getInstance().getCurrentUser().unlink(AGConnectAuthCredential.Phone_Provider)
                            .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<SignInResult>() {
                                @Override
                                public void onSuccess(SignInResult signInResult) {
                                    Toast.makeText(PhoneActivity.this, "解除手机号关联成功", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Toast.makeText(PhoneActivity.this, "解除手机号关联失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    private void sendVerificationCode() {
        String phoneNumber = accountEdit.getText().toString().trim();
        Myutils.sendVerificationCodeByPhoneNumber(PhoneActivity.this, send, phoneNumber, VerifyCodeSettings.ACTION_REGISTER_LOGIN);
    }
}