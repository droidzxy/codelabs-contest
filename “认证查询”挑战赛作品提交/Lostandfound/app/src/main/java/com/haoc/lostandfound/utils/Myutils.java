package com.haoc.lostandfound.utils;

import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.VerifyCodeResult;
import com.huawei.agconnect.auth.VerifyCodeSettings;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hmf.tasks.TaskExecutors;

import java.util.Locale;

/**
 * @description: 工具类
 * @author: Haoc
 * @Time: 2022/4/30  7:57
 */

public class Myutils {
    /**
     * 发送邮箱验证码
     *
     * @param context
     * @param button
     * @param email
     * @param action
     */
    public static void sendVerificationCodeByEmail(Context context, Button button, String email, int action) {
        //构造验证码settings
        VerifyCodeSettings settings = VerifyCodeSettings.newBuilder()
                .action(action)
                .sendInterval(60) //最短发送间隔，30-120s
                .locale(Locale.CHINA) //可选，必须包含国家和语言 eg:zh_CN
                .build();
        if (!email.isEmpty()) {
            Task<VerifyCodeResult> task = AGConnectAuth.getInstance().requestVerifyCode(email, settings);
            task.addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<VerifyCodeResult>() {
                @Override
                public void onSuccess(VerifyCodeResult verifyCodeResult) {
                    //new倒计时对象,总共的时间,每隔多少秒更新一次时间
                    CountDownTimerUtils countDownTimerUtilsextends
                            = new CountDownTimerUtils(button, 60000, 1000);
                    countDownTimerUtilsextends.start();
                    Toast.makeText(context, "发送邮箱验证码成功", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(context, "发送邮箱验证码失败:" + e, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(context, "请输入邮箱", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 发送手机验证码
     *
     * @param context
     * @param button
     * @param phoneNumber
     * @param action
     */
    public static void sendVerificationCodeByPhoneNumber(Context context, Button button, String phoneNumber, int action) {
        //构造验证码settings
        VerifyCodeSettings settings = VerifyCodeSettings.newBuilder()
                .action(action)
                .sendInterval(60) //最短发送间隔，30-120s
                .locale(Locale.CHINA) //可选，必须包含国家和语言 eg:zh_CN
                .build();
        if (!phoneNumber.isEmpty()) {
            Task<VerifyCodeResult> task = AGConnectAuth.getInstance().requestVerifyCode("86", phoneNumber, settings);
            task.addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<VerifyCodeResult>() {
                @Override
                public void onSuccess(VerifyCodeResult verifyCodeResult) {
                    //new倒计时对象,总共的时间,每隔多少秒更新一次时间
                    CountDownTimerUtils countDownTimerUtilsextends
                            = new CountDownTimerUtils(button, 60000, 1000);
                    countDownTimerUtilsextends.start();
                    Toast.makeText(context, "发送手机验证码成功", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(context, "发送手机验证码失败:" + e, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(context, "请输入手机号", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 修改手机号码
     *
     * @param context
     * @param phoneNumber
     * @param verifyCode
     */
    public static void updatePhone(Context context, String phoneNumber, String verifyCode) {
        if (AGConnectAuth.getInstance().getCurrentUser() != null) {
            AGConnectAuth.getInstance().getCurrentUser().updatePhone("86", phoneNumber, verifyCode)
                    .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(context, "更新手机号成功", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(context, "更新手机号失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    /**
     * 修改邮箱地址
     *
     * @param context
     * @param email
     * @param verifyCode
     */
    public static void updateEmail(Context context, String email, String verifyCode) {
        if (AGConnectAuth.getInstance().getCurrentUser() != null) {
            AGConnectAuth.getInstance().getCurrentUser().updateEmail(email, verifyCode)
                    .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(context, "更新邮箱成功", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(context, "更新邮箱失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    /**
     * 修改密码
     *
     * @param context
     * @param newPassword
     * @param verifyCode
     * @param provider
     */
    public static void updatePassword(Context context, String newPassword, String verifyCode, int provider) {
        if (AGConnectAuth.getInstance().getCurrentUser() != null) {
            AGConnectAuth.getInstance().getCurrentUser().updatePassword(newPassword, verifyCode, provider)
                    .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(context, "修改密码成功", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(context, "修改密码失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    /**
     * 重置邮箱密码
     *
     * @param context
     * @param email
     * @param verifyCode
     */
    public static void resetEmailPassword(Context context, String email, String verifyCode) {
        AGConnectAuth.getInstance().resetPassword(email, "123456", verifyCode)
                .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "重置密码成功，已初始化密码为123456", Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Toast.makeText(context, "重置密码失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * 重置手机密码
     *
     * @param context
     * @param phoneNumber
     * @param verifyCode
     */
    public static void resetPhonePassword(Context context, String phoneNumber, String verifyCode) {
        AGConnectAuth.getInstance().resetPassword("86", phoneNumber, "123456", verifyCode)
                .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "重置密码成功，已初始化密码为123456", Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Toast.makeText(context, "重置密码失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
