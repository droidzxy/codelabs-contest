package com.haoc.lostandfound.cloudstorage;

import android.net.Uri;
import android.util.Log;

import com.haoc.lostandfound.LostAndFoundApplication;
import com.haoc.lostandfound.auth.AuthMainActivity;
import com.huawei.agconnect.cloud.storage.core.AGCStorageManagement;
import com.huawei.agconnect.cloud.storage.core.StorageReference;
import com.huawei.agconnect.cloud.storage.core.UploadTask;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;

import java.io.File;

/**
 * @description: 云存储相关方法
 * @author: Haoc
 * @Time: 2022/4/25  15:38
 */

public class CloudStorage {
    private static final String TAG = "CloudStorage";
    private static AGCStorageManagement mAGCStorageManagement;
    private static String downloadUrl;

    //私有化构造方法
    private CloudStorage() {
    }

    //创建一个对象
    private static final CloudStorage cloudStorage = new CloudStorage();

    //提供public方法供外部访问，返回这个创建的对象
    public static CloudStorage getInstance() {
        return cloudStorage;
    }

    public static void initAGCStorageManagement() {
        mAGCStorageManagement = AGCStorageManagement.getInstance("cloudstorage-e0db4");
        Log.i(TAG, "initAGCStorageManagement success! ");
    }

    public void uploadFile(File file) {
        final String path =  "crop" + LostAndFoundApplication.uid + ".jpg";
        if (!file.exists()) {
            Log.w(TAG, "file is not exist! ");
            return;
        }
        if (mAGCStorageManagement == null) {
            initAGCStorageManagement();
        }
        StorageReference storageReference = mAGCStorageManagement.getStorageReference(path);
        UploadTask uploadTask = (UploadTask) storageReference.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.UploadResult>() {
                    @Override
                    public void onSuccess(UploadTask.UploadResult uploadResult) {
                        Log.i(TAG, "upload success! ");
                        getDownLoadUrl();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        Log.w(TAG, "upload failure! " + e.getMessage());
                    }
                });
    }

    public void getDownLoadUrl() {
        if (mAGCStorageManagement == null) {
            initAGCStorageManagement();
        }
        StorageReference storageReference = mAGCStorageManagement.getStorageReference("crop" + LostAndFoundApplication.uid + ".jpg");
        Task<Uri> task = storageReference.getDownloadUrl();
        task.addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                downloadUrl = uri.toString();
                if (!downloadUrl.isEmpty()) {
                    // 更新头像
                    AuthMainActivity.authMainActivity.updateAvatar(downloadUrl);
                }
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "get download url failed: " + e.getMessage());
            }
        });
    }
}
