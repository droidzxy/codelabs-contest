package com.haoc.lostandfound.auth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.auth.HwIdAuthProvider;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hmf.tasks.TaskExecutors;
import com.huawei.hms.common.ApiException;
import com.huawei.hms.support.account.AccountAuthManager;
import com.huawei.hms.support.account.request.AccountAuthParams;
import com.huawei.hms.support.account.request.AccountAuthParamsHelper;
import com.huawei.hms.support.account.result.AuthAccount;
import com.huawei.hms.support.account.service.AccountAuthService;

public class HWIDActivity extends ThirdBaseActivity {

    private static final String TAG = HWIDActivity.class.getSimpleName();
    private static final int SIGN_CODE = 9901;
    private static final int LINK_CODE = 9902;
    private AccountAuthService accountAuthService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AccountAuthParams authParams = new AccountAuthParamsHelper(AccountAuthParams.DEFAULT_AUTH_REQUEST_PARAM)
                .setAccessToken()
                .createParams();
        accountAuthService = AccountAuthManager.getService(HWIDActivity.this, authParams);
    }

    @Override
    public void login() {
        startActivityForResult(accountAuthService.getSignInIntent(), SIGN_CODE);
    }

    @Override
    public void link() {
        startActivityForResult(accountAuthService.getSignInIntent(), LINK_CODE);
    }

    @Override
    public void unlink() {
        if (AGConnectAuth.getInstance().getCurrentUser() != null) {
            // unlink huawei id
            AGConnectAuth.getInstance().getCurrentUser().unlink(AGConnectAuthCredential.HMS_Provider);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGN_CODE) {
            Task<AuthAccount> authAccountTask  = AccountAuthManager.parseAuthResultFromIntent(data);
            if (authAccountTask.isSuccessful()) {
                AuthAccount authAccount = authAccountTask.getResult();
                Log.i(TAG, "accessToken:" + authAccount.getAccessToken());
                // create huawei id credential
                AGConnectAuthCredential credential = HwIdAuthProvider.credentialWithToken(authAccount.getAccessToken());
                // sign in
                auth.signIn(credential)
                        .addOnSuccessListener(signInResult -> loginSuccess())
                        .addOnFailureListener(e -> showToast(e.getMessage()));
            } else {
                Log.e(TAG, "sign in failed : " + ((ApiException) authAccountTask.getException()).getStatusCode());
            }
        } else if (requestCode == LINK_CODE) {
            Task<AuthAccount> authAccountTask = AccountAuthManager.parseAuthResultFromIntent(data);
            if (authAccountTask.isSuccessful()) {
                AuthAccount huaweiAccount = authAccountTask.getResult();
                // create huawei id credential
                AGConnectAuthCredential credential = HwIdAuthProvider.credentialWithToken(huaweiAccount.getAccessToken());
                if (auth.getCurrentUser() != null) {
                    // link huawei id
                    auth.getCurrentUser().link(credential)
                            .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<SignInResult>() {
                                @Override
                                public void onSuccess(SignInResult signInResult) {
                                    showToast("link success");
                                }
                            }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            showToast(e.getMessage());
                        }
                    });
                }
            } else {
                Log.e(TAG, "sign in failed : " + ((ApiException) authAccountTask.getException()).getStatusCode());
            }
        }
    }
}