package com.example.contest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.auth.PhoneAuthProvider;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.agconnect.auth.VerifyCodeResult;
import com.huawei.agconnect.auth.VerifyCodeSettings;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hmf.tasks.TaskExecutors;

import java.util.Locale;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        final EditText loginText_num = findViewById(R.id.login_accout_nmm);
        //final EditText repsd_txt = findViewById(R.id.login_code);
        final String countryCodeStr = "86";
        final EditText loginVeri_code = findViewById(R.id.login_code);
        final String TAG = "RegisterActivity--";

        findViewById(R.id.login_code_obtain).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                String phoneNumberStr = loginText_num.getText().toString().trim();
                VerifyCodeSettings settings = new VerifyCodeSettings.Builder()
                        .action(VerifyCodeSettings.ACTION_REGISTER_LOGIN)
                        .sendInterval(30)
                        .locale(Locale.CHINA)
                        .build();
                Task<VerifyCodeResult> task = AGConnectAuth.getInstance().requestVerifyCode(countryCodeStr, phoneNumberStr, settings);
                task.addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<VerifyCodeResult>() {
                    @Override
                    public void onSuccess(VerifyCodeResult verifyCodeResult) {
                        //验证码申请成功
                        Log.d(TAG, "onSuccess: message send successfully"+phoneNumberStr);
                        Toast.makeText(LoginActivity.this,"send phone verify code success",Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        //失败就销户
                        //AGConnectAuth.getInstance().deleteUser();
                        Log.e(TAG, "onSuccess: message send failed"+phoneNumberStr);
                        Toast.makeText(LoginActivity.this, "requestVerifyCode fail:" + e, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumberStr = loginText_num.getText().toString().trim();
                String vericode = loginVeri_code.getText().toString().trim();
                AGConnectAuthCredential credential = PhoneAuthProvider.credentialWithVerifyCode(countryCodeStr,phoneNumberStr,"",vericode);
                AGConnectAuth.getInstance().signIn(credential)
                        .addOnSuccessListener(new OnSuccessListener<SignInResult>() {
                            @Override
                            public void onSuccess(SignInResult signInResult) {
                                //获取登录信息
                                Intent intent = new Intent();
                                intent.setClass(LoginActivity.this, DatabaseActivity.class);
                                startActivity(intent);
                                Log.i(TAG, "onSuccess: login success");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {
                            }
                        });
            }
        });

    }
}