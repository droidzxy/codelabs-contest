package com.army.codelabs;

import com.army.codelabs.slice.MainAbilitySlice;
import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.applinking.AGConnectAppLinking;
import com.huawei.hms.analytics.HiAnalytics;
import com.huawei.hms.analytics.HiAnalyticsInstance;
import com.huawei.hms.analytics.HiAnalyticsTools;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.utils.net.Uri;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

        // 在应用的MainAbility的onStart()方法中初始化AGC基础SDK
        AGConnectInstance.initialize(getAbilityPackage());

        // 打开SDK日志开关
        HiAnalyticsTools.enableLog();
        HiAnalyticsInstance instance = HiAnalytics.getInstance(this);


//        AGConnectAppLinking.getInstance().getAppLinking(this).addOnSuccessListener(resolvedLinkData -> {
////            Uri deepLink = null;
////            if (resolvedLinkData!= null) {
////                deepLink = resolvedLinkData.getDeepLink();
////            }
//            //根据Deep Link信息来进行后续处理，比如打开详情页面或配置页面等。
//        });
    }
}
