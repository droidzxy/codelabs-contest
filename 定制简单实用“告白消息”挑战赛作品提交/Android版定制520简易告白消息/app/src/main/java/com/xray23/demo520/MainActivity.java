package com.xray23.demo520;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.huawei.agconnect.appmessaging.AGConnectAppMessaging;
import com.huawei.agconnect.appmessaging.model.Action;
import com.huawei.agconnect.appmessaging.model.AppMessage;
import com.huawei.agconnect.remoteconfig.AGConnectConfig;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hms.aaid.HmsInstanceId;
import com.huawei.hms.aaid.entity.AAIDResult;

import java.io.InputStream;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "demo520消息";

    // 默认远程配置
    // key
    private static final String REMOTE_KEY = "banner_img";
    // value
    private static final String REMOTE_VALUE = "new_banner";
    // 调用远程配置更新的间隔
    private long fetchInterval = 0;

    private AGConnectAppMessaging appMessaging;
    private AGConnectConfig appConfig;

    // banner图片
    private ImageView imageView;
    //云存储中banner图片url地址
    private static final String URL = "https://blog.tengfei.link/inAppMsg/1653745181158/520.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 获取ImageView对象
        imageView = findViewById(R.id.img_view);

        // 获取aaid
        getAAID();
        // 应用内消息
        configMessage();
        // 远程配置
        configRemote();

    }

    private void configRemote() {
        // 实例化
        appConfig = AGConnectConfig.getInstance();

        appConfig.fetch(fetchInterval).addOnSuccessListener(configValues -> {
            // 配置参数生效
            appConfig.apply(configValues);

            //以String方式获取参数值
            String value = appConfig.getValueAsString(REMOTE_KEY);

            if (value.equals(REMOTE_VALUE)) {
                // 使用Glide图片框架更改图片（需要在AndroidManifest.xml中加入网络访问权限）
                Glide.with(MainActivity.this).load(URL).into(imageView);
            }

        }).addOnFailureListener(e -> {
            Toast.makeText(getBaseContext(), "Fetch Fail", Toast.LENGTH_LONG).show();
        });
    }

    /**
     * 配置应用内消息
     */
    private void configMessage() {
        // 实例化
        appMessaging = AGConnectAppMessaging.getInstance();
        // 设置是否允许同步AGC服务端数据
        appMessaging.setFetchMessageEnable(true);
        // 强制请求AGC服务端消息数据
        appMessaging.setForceFetch("AppOnForeground");
        // 设置是否允许展示消息
        appMessaging.setDisplayEnable(true);
        // 消息展示监听器
        appMessaging.addOnDisplayListener((appMessage) -> {
                    System.out.println("展示消息成功");
                    Toast.makeText(MainActivity.this, "展示消息成功", Toast.LENGTH_SHORT).show();
                }
        );
        // 消息点击监听器
        appMessaging.addOnClickListener((AppMessage appMessage, Action action) -> {
                    //点击后打开弹框消息设置的url
                    String urlStr = action.getActionUrl();
                    Log.i(TAG, "getActionUrl: card url" + urlStr);
                    Uri url = Uri.parse(urlStr);
                    Log.i(TAG, "onMessageClick: message clicked" + url);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(url);
                    startActivity(intent);
                }
        );
    }

    /**
     * 获取AAID
     */
    private void getAAID() {
        Task<AAIDResult> idResult = HmsInstanceId.getInstance(getApplicationContext()).getAAID();
        idResult.addOnSuccessListener(new OnSuccessListener<AAIDResult>() {
            @Override
            public void onSuccess(AAIDResult aaidResult) {
                // 获取AAID方法成功
                String aaid = aaidResult.getId();
                Log.d(TAG, "getAAID successfully, aaid is " + aaid);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception myException) {
                // 获取AAID失败
                Log.d(TAG, "getAAID failed, catch exception: " + myException);
            }
        });
    }

    /**
     * 加载imgUrl
     *
     * @param url
     * @return
     */
    private void loadImageFromUrl(String url) {
        new Thread(() -> {
            try {
                InputStream is = (InputStream) new URL(url).getContent();
                Drawable d = Drawable.createFromStream(is, "banner图片地址");
                imageView.setImageDrawable(d);
            } catch (Exception e) {
                System.out.println("Exc=" + e);
            }
        }).start();

    }
}